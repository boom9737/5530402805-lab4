package kitteerapakorn.attaphon.lab4;

public class ThaiPlayer extends Player {
	private String SportName;

	ThaiPlayer(String name, int dDay, int dMonth, int dYear, double weight,
			double height) 
	{
		super(name, dDay, dMonth, dYear, weight, height);
		// TODO Auto-generated constructor stub
	}
	
	public void setSportName(String newSportName) 
	{
		SportName = newSportName;
	}
	
	public String getSportName()
	{
		return SportName;
	}
	
	public String toString()
	{
		
		
		return "Player [" + this.getName() + " is " + 
					this.Year() + " year old, with weight is " + Weight +
					" and height is " + Height + "]\n" + "[ nationality = Thai" + (SportName != null ? "award = " + this.getSportName() : "") + " ]";
					
	/*	if (SportName == null)
		return "Player [" + this.getName() + " is " + 
							this.Year() + " year old, with weight is " + Weight +
								" and height is " + Height + "]\n" + "[ nationality = Thai ]" ;
		else
			return "Player [" + this.getName() + " is " + 
						this.Year() + " year old, with weight is " + Weight +
					" and height is " + Height + "]\n" + "[ nationality = Thai " + "award = " + this.getSportName() + " ]";
	*/
	}

}
