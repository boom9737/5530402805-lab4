package kitteerapakorn.attaphon.lab4;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Player
{
	private String Name;
	protected double Height;
	protected double Weight;
	public int dDay, dMonth, dYear;
	int res;
	
	Player(String name, int dDay,int dMonth, int dYear, double weight, double height)
	{
		Name = name;
		this.dDay = dDay;
		this.dMonth = dMonth;
		this.dYear = dYear;
		Weight = weight;
		Height = height;
	}
	
	public void setName(String newName)
	{
		Name = newName;
	}
	
	public String getName()
	{
		return Name;
	}
	
	public void setHeight(double newHeight)
	{
		Height = newHeight;
	}
	
	public double getHeight()
	{
		return Height;
	}
	
	public void setWeight(double newWeight)
	{
		Weight = newWeight;
	}
	
	public double getWeight()
	{
		return Weight;
	}
	
	public void setBD(int d, int m, int y)
	{
		dDay = d;
		dMonth = m;
		dYear = y;
	}
	
	
	  public int Year() {
		    Calendar cal = new GregorianCalendar(dYear, dMonth, dDay);
		    Calendar now = new GregorianCalendar();
		    res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		    if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
		        || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal.get(Calendar.DAY_OF_MONTH) > now
		            .get(Calendar.DAY_OF_MONTH))) {
		      res--;
		    }
		    return res;
		  }
	
	
	public String toString()
	{
		
		return "Player [" + this.getName() + " is " + 
					this.Year() + " year old, with weight is " + Weight +
					" and height is " + Height + "]";
	}
}
